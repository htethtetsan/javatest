package com.example.test.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class UserDto {

    @NotEmpty(message = "name ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String name;

    @NotEmpty(message = "phoneNo ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String phoneNo;

    @NotEmpty(message = "email ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String email;

    @NotEmpty(message = "password ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String password;
}
