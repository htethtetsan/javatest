package com.example.test.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class StudentEditDto {
    @NotEmpty(message = "boId ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String boId;

    private String name;

    private String dateOfBirth;

    private String rollNo;

    private String grade;

    private String teachingClass;

    private String parentPhoneNo;

    private String address;


}
