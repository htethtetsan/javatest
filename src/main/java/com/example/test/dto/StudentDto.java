package com.example.test.dto;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;


@Getter
@Setter
public class StudentDto {

    @NotEmpty(message = "name ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String name;

    private String dateOfBirth;

    @NotEmpty(message = "rollNo ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String rollNo;

    @NotEmpty(message = "Grade ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String grade;

    @NotEmpty(message = "Class ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String teachingClass;

    private String parentPhoneNo;

    private String address;
}
