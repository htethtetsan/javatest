package com.example.test.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class LoginDto {
    @NotEmpty(message = "email ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String email;

    @NotEmpty(message = "password ထည့်ရန်ကျန်ရှိနေပါသေးသည်!")
    private String password;
}
