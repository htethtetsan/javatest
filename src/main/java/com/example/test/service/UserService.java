package com.example.test.service;

import com.example.test.dto.UserDto;
import com.example.test.entity.Response;

public interface UserService {

    public Response create(UserDto request);

    public Response login(String email, String password);


    }
