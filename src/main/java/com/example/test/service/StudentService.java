package com.example.test.service;

import com.example.test.dto.StudentDto;
import com.example.test.dto.StudentEditDto;
import com.example.test.entity.Response;

public interface StudentService {

    public Response create(StudentDto request);

    public Response getList(int pageNo, int pageSize);

    public Response update(StudentEditDto request);

    public Response delete(String boId);



    }
