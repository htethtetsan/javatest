package com.example.test.service.impl;

import com.example.test.dto.StudentDto;
import com.example.test.dto.StudentEditDto;
import com.example.test.entity.EntityStatus;
import com.example.test.entity.Response;
import com.example.test.entity.Student;
import com.example.test.repository.StudentRepository;
import com.example.test.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service("studentService")
public class StudentServiceImpl implements StudentService {

    private static Logger logger = Logger.getLogger(UserServiceImpl.class);
    @Autowired
    private StudentRepository studentRepository;

    public void save(Student student) {
        try {
            if (student.isBoIdRequired(student.getBoId()))
                student.setBoId("STUDENT" + studentRepository.count());
            studentRepository.save(student);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
        }
    }

    public Response create(StudentDto request) {
        save(new Student(request));
        return new Response(true, "success");
    }

    public Response getList(int pageNo, int pageSize) {
        Pageable page = PageRequest.of(pageNo == 1 ? 0 : pageNo - 1, pageSize, Sort.by(Sort.Direction.DESC, "id"));
        Page<Student> studentPage = studentRepository.findByEntityStatus(EntityStatus.ACTIVE, page);

        Response response = new Response(true, "success");
        response.setData(studentPage.getContent());
        response.setLastPage(studentPage.getTotalPages());
        return response;
    }

    public Response update(StudentEditDto request) {
        Student student = studentRepository.findByBoIdAndEntityStatus(request.getBoId(), EntityStatus.ACTIVE);
        if (student == null)
            return new Response(false, "Student isn't found.");
        student.edit(request);
        save(student);
        return new Response(true, "success");
    }

    public Response delete(String boId){
        Student student = studentRepository.findByBoIdAndEntityStatus(boId,EntityStatus.ACTIVE);
        if (student == null)
            return new Response(false, "Student isn't found.");

        student.setEntityStatus(EntityStatus.DELETED);
        save(student);
        return new Response(true, "success");
    }
}
