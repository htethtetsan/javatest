package com.example.test.service.impl;

import com.example.test.dto.UserDto;
import com.example.test.entity.EntityStatus;
import com.example.test.entity.Response;
import com.example.test.entity.User;
import com.example.test.repository.UserRepository;
import com.example.test.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {
    private static Logger logger = Logger.getLogger(UserServiceImpl.class);
    @Autowired
    private UserRepository userRepository;

    public void save(User user) {
        try {
            if (user.isBoIdRequired(user.getBoId()))
                user.setBoId("USR" + userRepository.count());
            userRepository.save(user);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
        }
    }

    public Response create(UserDto request) {
        User user = userRepository.findByEmailAndEntityStatus(request.getEmail(), EntityStatus.ACTIVE);
        if (user != null)
            return new Response(false, "This email is already registered.");
        save(new User(request));
        return new Response(true, "success");
    }

    public Response login(String email, String password) {
        User user = userRepository.findByEmailAndEntityStatus(email, EntityStatus.ACTIVE);
        if (user == null)
            return new Response(false, "User isn't found. Please register first.");
        if (!password.equals(user.getPassword()))
            return new Response(false, "Password is wrong");
        return new Response(true, "Login Success!");
    }
}
