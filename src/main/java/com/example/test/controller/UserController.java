package com.example.test.controller;

import com.example.test.config.JwtUtil;
import com.example.test.dto.LoginDto;
import com.example.test.dto.UserDto;
import com.example.test.entity.Response;
import com.example.test.service.UserService;
import com.example.test.service.impl.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping("user")
public class UserController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailService userDetailService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/create")
    public Response create(@RequestBody @Valid UserDto request) throws Exception {
        return userService.create(request);
    }

    @PostMapping("/login")
    public Response login(@RequestBody @Valid LoginDto request) throws Exception {
        String userName = request.getEmail();
        Response response = userService.login(userName, request.getPassword());
        if (!response.isStatus())
            return response;

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, request.getPassword()));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
        final UserDetails userDetails = userDetailService.loadUserByUsername(userName);
        final String jwtToken = jwtUtil.generateToken(userDetails.getUsername());
        response.setAccessToken(jwtToken);
        response.setEmail(userName);
        return response;
    }
}
