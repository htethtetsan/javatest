package com.example.test.controller;

import com.example.test.dto.StudentDto;
import com.example.test.dto.StudentEditDto;
import com.example.test.entity.Response;
import com.example.test.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping("student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/create")
    public Response create(@RequestBody @Valid StudentDto request) throws Exception {
        return studentService.create(request);
    }

    @GetMapping("/hello")
    public String getList() {
        return "hello";
    }

    @GetMapping("/list")
    public Response getList(@RequestParam int pageNo, @RequestParam(defaultValue  = "10") Integer pageSize) throws Exception {
        return studentService.getList(pageNo, pageSize);
    }

    @PostMapping("/update")
    public Response update(@RequestBody @Valid StudentEditDto request) throws Exception {
        return studentService.update(request);
    }

    @GetMapping("/delete")
    public Response delete(@RequestParam String boId) throws Exception {
        return studentService.delete(boId);
    }

}
