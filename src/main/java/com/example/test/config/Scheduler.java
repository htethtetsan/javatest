package com.example.test.config;

import com.example.test.entity.EntityStatus;
import com.example.test.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalTime;

@Configuration
@EnableScheduling
public class Scheduler {

    @Autowired
    private StudentRepository studentRepository;

    // @Scheduled(cron = "* * * ? * *" ) //every second
    @Scheduled(cron = "0 0 * ? * *") //every hour
    public void logStudentCount() {
        System.out.println(LocalTime.now() + " Student Count:  " + studentRepository.findByEntityStatus(EntityStatus.ACTIVE).size());
    }
}
