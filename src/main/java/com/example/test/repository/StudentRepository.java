package com.example.test.repository;

import com.example.test.entity.EntityStatus;
import com.example.test.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long>, JpaSpecificationExecutor<Student> {

    public Page<Student> findByEntityStatus(EntityStatus entityStatus, Pageable page);

    public Student findByBoIdAndEntityStatus(String boId, EntityStatus entityStatus);

    public List<Student> findByEntityStatus(EntityStatus entityStatus);
}
