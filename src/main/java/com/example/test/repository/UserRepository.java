package com.example.test.repository;

import com.example.test.entity.EntityStatus;
import com.example.test.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository  extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    public User findByEmailAndEntityStatus(String email, EntityStatus entityStatus);
}
