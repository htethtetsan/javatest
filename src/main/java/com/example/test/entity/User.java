package com.example.test.entity;

import com.example.test.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "userdetail")
public class User extends AbstractEntity {

    @JsonView(Views.Thin.class)
    private String name;

    @JsonView(Views.Thin.class)
    private String phoneNo;

    @JsonView(Views.Thin.class)
    private String email;

    @JsonView(Views.Thin.class)
    private String password;

    public User(UserDto request) {
        setName(request.getName());
        setPhoneNo(request.getPhoneNo());
        setEmail(request.getEmail());
        setPassword(request.getPassword());
    }
}
