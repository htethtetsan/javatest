package com.example.test.entity;

public enum EntityStatus {
	ACTIVE, INACTIVE, DELETED, NEW, EXPIRED;
}
