package com.example.test.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Response<T> implements Serializable {

    private static final long serialVersionUID = 8992463810222512826L;

    @JsonView(Views.Thin.class)
    private T data;

    @JsonView(Views.Thin.class)
    private String email;

    @JsonView(Views.Thin.class)
    private String accessToken;

    @JsonView(Views.Thin.class)
    private boolean status;

    @JsonView(Views.Thin.class)
    private String  message;

    @JsonView(Views.Thin.class)
    private int lastPage;

    public Response(String email, String accessToken) {
        setEmail(email);
        setAccessToken(accessToken);
    }

    public Response (boolean status, String message){
        setStatus(status);
        setMessage(message);
    }

}
