package com.example.test.entity;

import com.example.test.dto.StudentDto;
import com.example.test.dto.StudentEditDto;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "student")
public class Student extends AbstractEntity {

    @JsonView(Views.Thin.class)
    private String name;

    @JsonView(Views.Thin.class)
    private String dateOfBirth;

    @JsonView(Views.Thin.class)
    private String rollNo;

    @JsonView(Views.Thin.class)
    private String grade;

    @JsonView(Views.Thin.class)
    private String teachingClass;

    @JsonView(Views.Thin.class)
    private String parentPhoneNo;

    @JsonView(Views.Thin.class)
    private String address;

    public Student(StudentDto request){
        setName(request.getName());
        setDateOfBirth(request.getDateOfBirth());
        setRollNo(request.getRollNo());
        setGrade(request.getGrade());
        setTeachingClass(request.getTeachingClass());
        setParentPhoneNo(request.getParentPhoneNo());
        setAddress(request.getAddress());
    }

    public void edit(StudentEditDto request){
        setName(request.getName().isEmpty()? getName() : request.getName());
        setDateOfBirth(request.getDateOfBirth().isEmpty()? getDateOfBirth() : request.getDateOfBirth());
        setRollNo(request.getRollNo().isEmpty()? getRollNo() : request.getRollNo());
        setGrade(request.getGrade().isEmpty()? getGrade() : request.getGrade());
        setTeachingClass(request.getTeachingClass().isEmpty()? getTeachingClass() : request.getTeachingClass());
        setParentPhoneNo(request.getParentPhoneNo().isEmpty()? getParentPhoneNo() : request.getParentPhoneNo());
        setAddress(request.getAddress().isEmpty()? getAddress() : request.getAddress());
    }
}
