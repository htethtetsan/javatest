package com.example.test.entity;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public class AbstractEntity implements Serializable {

    private static final long serialVersionUID = -6529114573373018014L;

    private static final String boId_Required = "-1";

    @Id
    @JsonView(Views.Thin.class)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @JsonView(Views.Thin.class)
    @Column(name = "boId", nullable = false)
    private String boId = "";

    @Column(name = "entityStatus")
    @Enumerated(EnumType.STRING)
    private EntityStatus entityStatus;

    public AbstractEntity(){
        setBoId(boId_Required);
        setEntityStatus(EntityStatus.ACTIVE);
    }

    public boolean isBoIdRequired(String boId){
        return boId.equals(boId_Required);
    }
}
